<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class pluginEnquiry extends Notification
{
    use Queueable;

    public function __construct($form)
    {
        $this->form = $form;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        $form = $this->form;
        return (new MailMessage)->from('application@hasotech.com')->markdown('notifications.pluginEnquiry',['form' => $form]);
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
