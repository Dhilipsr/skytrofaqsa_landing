<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>@yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="icon" href="{{asset('img/Favicon_ SKYTROFA_03-03.png')}}" type="image/gif" sizes="16x16">
    <link rel="apple-touch-icon" href="{{asset('icon.png')}}">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    @yield('css')
  
   
   <!-- Google Tag Manager -->

<!-- End Google Tag Manager -->
</head> 
   

<!-- <script>-->
     
<!--     function myFunction() {-->
<!--    alert("dfsafdfs");-->
<!--  document.getElementById("ide").style.margin-top: 45px;-->
<!--}</script>-->
<!--<script>$(document).ready(function() {-->

<!--  $(".gaIsi").click(function () { -->
  
<!--  alert("gdfdgdgdg");-->
<!--  });-->
<!--}):-->
  
<!--  </script>-->
  


<body>
   
    <!-- Google Tag Manager (noscript) -->

<!-- End Google Tag Manager (noscript) -->
    
  
  
    <!-- Header Starts -->
    @include('partials.header')
    <!-- Header Ends -->

    <!-- Main Section -->
    @yield('content')
    <!-- Main Section Over-->


    <!-- Footer Starts -->
    @include('partials.footer')
    <!-- Footer Ends -->


    <!-- Modal Starts here-->
    @include('partials.modals')
    <!-- Modal Ends Here -->
   
    <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    @yield('js')
</body>

</html>