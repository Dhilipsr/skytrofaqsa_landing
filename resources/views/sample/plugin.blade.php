<!doctype html>
<html class="no-js" lang="">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>SKYTROFA – QSA – SAMPLE PLUGIN</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="manifest" href="site.webmanifest">
    <link rel="apple-touch-icon" href="sky_favicon-01-01.jpg">
    <link rel="icon" href="{{asset('img/Favicon_ SKYTROFA_03-03.png')}}">
    <!-- Place favicon.ico in the root directory -->
    <link rel="stylesheet" href="{{asset('sample/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('sample/css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/sampleMain.css')}}">
<style type="">
    li#sum.cris::marker
    {
        padding-left: 1em;
    }
</style>
<style>
    .row {
   /* display: -ms-flexbox;*/
    display: flex;
   /* -ms-flex-wrap: wrap;*/
    flex-wrap: wrap;
    margin-right: -15px;
    margin-left: -15px;


}
.bgC {
    background: #a02c74;
    height: 137px;
    max-width: 1140px;
}
</style>
</head>

<body>
   

    <!-- Header Starts -->

    <div class="frame ">
        <div class="samp-img"><i>sample</i></div>
        {{-- <div class="manu-sec">
            <div class="mob-menu-box">

                <div class="menu-icon"><span></span>
                    <p>menu</p>
                </div>
                <div class="mamber-logo"><img src="{{asset('img/man-logo.png')}}"></div>
            </div>
        </div>
        <div class="mob-menu-up">
            <div class="mob-menu">
                <div class="mob-close-box"><span class="mob-close"><img src="img/close-white.png" alt="img"></span>
                </div>
                <ul class="navigation">
                    <li><a href="login.html">Log In</a></li>

                </ul>
            </div>
        </div> --}}




        <header class="">
            <div class="side-space">
                <section class="container">
                    <div class="row">
                        <div class="col-12  ">
                            <div class="bgC">
                                <div class="logo" style="padding"><img src="{{asset('img/top_logo.png')}}">
                                </div>
                               
                                <div class="indication">
                                    <h6>Indications
                                    </h6>SKYTROFA<sup>®</sup is a human growth hormone indicated for the

treatment of pediatric patients 1 year and older who weigh 
at least 11.5 kg and have growth failure due to inadequate 
secretion of endogenous growth hormone (GH).


                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </header>
       
        <!-- Header Ends -->



        <!-- Main Section -->
        

       <form class="form">
            <section class="side-space main-section">
                <section class="container">
                    <div class="row">
                        <div class="col-12">
                            <nav class="top-link ">
                                <ul>
                                    <li class="pglink"><a href="#provider">Provider</a></li>
                                    <li class="pglink"><a href="#patients"> Patient</a></li>
                                  <!--   {{ route('pi') }} -->
                                    <li><a href="https://ascendispharma.us/products/pi/skytrofa/skytrofa_pi.pdf"
                                            target="_blank">Prescribing Information</a></li>
                                    <li><a href="#isi" data-scroll> Important Safety Information</a></li>
                                </ul>
                            </nav>
                        </div>
                        <div class="col-12">
                            <nav class="notification">
                                <ul>
                                    <li><img src="{{asset('sample/img/select.png')}}"> Select tool(s)</li>
                                    <li><img src="{{asset('sample/img/search.png')}}"> Preview tool</li>
                                    <li><img src="{{asset('img/arrow.png')}}"> Description</li>

                                </ul>
                            </nav>
                        </div>
                    </div>

                    <div class="row headings " id="provider">
                        <div class="col-6">
                            <div class="mr-left fsdf">
                                <span >
                                <h3 class="pro">PROVIDER</h3>
                            </span>
                            </div>
                        </div>
                      
                    </div>
                    <div class="row">
                        <div class="col-12  mr-left">
                          
                        </div>
                    </div>


                    
                        
                        
                    <div class="row ">
                        <div class="col-12 ">
                            <div class="pd-left">
                                <div class="acc pd-left">
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="01"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Considerations for PAs">
                                            
                                              <label class="dfdd" for="01"></label> <div class="adn" >SKYTROFA<sup>®</sup> Specialty Pharmacy Flashcard</div></label>
                                                     
                                            </span>
                                            <a href="https://cc-oge.online/asc-sky/skytrofa-specialty-pharmacy-flashcard-sample-6994"
                                        
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                          
                                           Contains important information about how to contact the Ascendis Pharma 
Signature Access Program<sup>TM</sup>(A•S•A•P) to get the Auto-Injector for patients.
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="02"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Counseling Tips">
                                                   
                                                
                                                      <label class="dfdd" for="02"></label> <div class="adn" >SKYTROFA<sup>®</sup> Dosing Options</div></label>
                                                     
                                                    
                                            </span> 
                                            <a href="https://cc-oge.online/asc-sky/skytrofa-dosing-sample-7183"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                               
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left">
                                          
                                          
                                            You’ll find straightforward dosing recommendations and a simple calculation 
for individualized dosing along with recommendations to make switching to 
once-weekly SKYTROFA easy.
                                        </div>
                                    </div>
                                    
                                     <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="02"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Counseling Tips">
                                                   
                                                
                                                      <label class="dfdd" for="02"></label> <div class="adn">Get Ready to Start e-Prescribing SKYTROFA<sup>®</sup> </div></label>
                                                     
                                                   
                                            </span> 
                                            <a href="https://cc-oge.online/asc-sky/get-ready-to-start-e-prescribing-sample-7207"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                               
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left">
                                          
                                          
                                          Learn how to make sure SKYTROFA is set up properly in your EHR to make e-prescribing SKYTROFA as seamless as possible.
                                        </div>
                                    </div>
                                    
                                    
                       
                                    
                                   <!-- this is i implemented down -->
                                    <label for="02" class="fillable-form">
                                        Fillable Forms
                                    </label>
                                       <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="03"
                                                    data-tool-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc"
                                                    data-print-url="https://cc-oge.online/ace-toc-unchc/patient-discharge-instructions-unchc-print"
                                                    data-title="Counseling Tips">
                                                    
                                                     <label class="dfdd" for="03"></label> <div class="adn" >Statement of Medical Necessity form</div></label>
                                           
                                            </span>
                                            <a href="https://cc-oge.online/asc-sky/statement-of-medical-necessity-form-sample-6998"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                               
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                       <div class="acc__panel  mr-left">
                                           <!--  Your eligible patients may be entitled to medications for no more than
                                            $8.95/prescription, which is a Medicare Part D
                                            benefit. -->This fillable verification form will be right at your fingertips when you're 
ready to prescribe SKYTROFA.
                                        </div>
                                    
                                    </div>
                                    <div class="acc__card">
                                 
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                         <section class="container bg-grey">
                         <div class="education-grey">
                           Educational resources for patients and their caregivers about<br>
                      <span class="chronic-txt" >managing Pediatric Growth Hormone Deficiency with SKYTROFA.</span>
                        </div>
                         </section>


                     <div class="row headings " id="patients">
                        <div class="col-6">
                            <div class="mr-left jjg">
                                <h3 class="pro">PATIENT</h3>
                            </div>
                        </div>
                      
                    </div>


             
 
                    <div class="row ">
                        <div class="col-12 ">
                            <div class="pd-left">
                                <div class="acc pd-left">
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="05">
                                                <label for="05">SKYTROFA Patient Brochure</label>
                                            </span>
                                            <a href="https://cc-oge.online/asc-sky/skytrofa-patient-brochure-sample-7181"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>
                                        <div class="acc__panel  mr-left">
                                           Give your patients the information they need to know about SKYTROFA, the 
SKYTROFA Auto-Injector and the Ascendis Signature Access Program (A•S•A•P)
personalized patient support.</span>
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                        <div class="acc__title">
                                            <span class="form-group">
                                                <input type="checkbox" name="selector[]" class="checkbox" id="06">
                                               
                                                
                                                
                                                  <label class="dfdd" for="06"></label> <div class="adn" >Ascendis Signature Access Program (A•S•A•P<sup>&trade;</sup>) Patient Brochure</div></label>
                                                     
                                            </span>
                                            <a href="https://cc-oge.online/asc-sky/ascendis-signature-access-program-(a.s.a.p)-patient-brochure-sample-7000"
                                                target="_blank" class="preview">
                                                <img src="{{asset('sample/img/search.png')}}">
                                            </a> <a class="drop-down"><img src="img/arrow.png"></a>
                                        </div>

                                        <div class="acc__panel  mr-left ">
                                           Your patients will find all the information they need about the services and support 
provided by the A•S•A•P program, including insurance and financial support, 
auto-injector delivery and training, and how to get additional information and 
support if they need it.
                                        </div>
                                    </div>
                                    <div class="acc__card">
                                      

                                </div>

                            </div>

                        </div>
                    </div>

                </section>
            </section>


                     
                    <div id="tab-2" class="tab-content ">
                        <div class="row headings ">
                            <div class="col-md-8">
                                <div class="mr-left">

                                    <h3>Caregiver and Patient 02</h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12  mr-left">
                                <div class="pd-left sub-title">
                                    <h3>Resources 02</h3>
                                </div>
                            </div>
                        </div>

                        <div class="row ">
                            <div class="col-12 ">
                                <div class="pd-left">
                                    <div class="acc pd-left">
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="07">
                                                    <label for="07"> Instant Savings Card

                                                        <small class="small-info">PLEASE NOTE: This is an online form
                                                            that
                                                            allows you to download the card. It is not available
                                                            in Spanish.</small>
                                                    </label>

                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>
                                            <div class="acc__panel  mr-left">
                                                Provides most eligible, commercially insured patients help with their
                                                monthly
                                                co-pays for. <span class="note">PLEASE NOTE: This tool can not
                                                    be
                                                    printed.</span>
                                            </div>
                                        </div>
                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="08">
                                                    <label for="08">Reduce the Risk of OHE Recurrence With
                                                        </label>
                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>

                                            <div class="acc__panel  mr-left">
                                                Provides information about  coverage, prescription savings and
                                                how to
                                                sign up
                                                for the HE Living Program (H.E.L.P.).

                                            </div>
                                        </div>

                                        <div class="acc__card">
                                            <div class="acc__title">
                                                <span class="form-group">
                                                    <input type="checkbox" name="selector[]" class="checkbox" id="09">
                                                    <label for="09">Setting up Medical Alerts on Digital Devices</label>
                                                </span>
                                                <a class="preview"><img src="{{asset('sample/img/search.png')}}"></a> <a
                                                    class="drop-down"><img src="img/arrow.png"></a>
                                            </div>

                                            <div class="acc__panel  mr-left">
                                                Instructions for setting up alerts on an iPhone, android or Apple Watch.
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                        </div>
                       
                    </div>





                </section>
                <!-- BG-BLUE added by me down -->
                <section class="BG-BLUE">
                <section class="container">
                    <a class="back-to-top">
                        Back to Top <img src="img/imagetop.png" class="bt">
                    </a>
                    <!--   <hr class="gr-divi"> -->
                </section>


                <section class="container">
                    <div class="row ">
                        <div class="col-12 d-flex justify-content-center action">
                            <div href="#" data-toggle="modal" data-target="#myModal" class="button">Email</div>
                            <div class="button" data-toggle="modal" data-target="#print">View/Print
                            </div>
                            <div class="button" data-toggle="modal" data-target="#copy" id="copy-tool">Copy Link</div>
                        </div>
                    </div>
                </section>
                    </section>

                <section class="para">
                    <div class="row ">
                        <div class="col-12 imp-safety">
                            <h6>INDICATION</h6>
                            <div class="war">SKYTROFA<sup>®</sup> is a human growth hormone indicated for the treatment of pediatric patients 1 year and older 
who weigh at least 11.5 kg and have growth failure due to inadequate secretion of endogenous growth 
hormone (GH).</div>
                            <h6 class="head" id="isi">IMPORTANT SAFETY INFORMATION</h6>
                            <ul>
                                <li class="cris">
                                    SKYTROFA is contraindicated in patients with:
                                </li>
                                <ul>
                                    <li class="cris" id="sum">
                                        Acute critical illness after open heart surgery, abdominal surgery or multiple accidental trauma, or if 
you have acute respiratory failure due to the risk of increased mortality with use of armacologic 
doses of somatropin.
                                    </li>
                                    <li class="cris" id="sum">
                                        Hypersensitivity to somatropin or any of the excipients in SKYTROFA. Systemic hypersensitivity 
reactions have been reported with post-marketing use of somatropin products.
                                    </li>
                                    <li class="cris" id="sum">
                                        Closed epiphyses for growth promotion.
                                    </li>
                                    <li class="cris" id="sum">
                                        Active malignancy.
                                    </li>
                                    <li class="cris" id="sum">
                                        Active proliferative or severe non-proliferative diabetic retinopathy.
                                    </li>
                                    <li class="cris" id="sum">
                                        Prader-Willi syndrome who are severely obese, have a history of upper airway obstruction or 
sleepapnea or have severe respiratory impairment due to the risk of sudden death.
                                    </li>
                                </ul>
                                 <li class="cris">
                                        Increased mortality in patients with acute critical illness due to complications following open heart 
surgery, abdominal surgery or multiple accidental trauma, or those with acute respiratory failure has 
been reported after treatment with pharmacologic doses of somatropin. Safety of continuing SKYTROFA 
treatment in patients receiving replacement doses for the approved indication who concurrently develop 
these illnesses has not been established
                                 </li>
                                 <li class="cris">
                                         Serious systemic hypersensitivity reactions including anaphylactic reactions and angioedema have 
been reported with post-marketing use of somatropin products. Do not use SKYTROFA in patients with 
known hypersensitivity to somatropin or any of the excipients in SKYTROFA.
                                 </li>
                                 <li class="cris">
                                        There is an increased risk of malignancy progression with somatropin treatment in patients with active 
malignancy. Preexisting malignancy should be inactive with treatment completed prior to starting 
SKYTROFA. Discontinue SKYTROFA if there is evidence of recurrent activity
                                 </li>
                                 <li class="cris">
                                         In childhood cancer survivors who were treated with radiation to the brain/head for their first neoplasm 
and who developed subsequent growth hormone deficiency (GHD) and were treated with somatropin, an 
increased risk of a second neoplasm has been reported. Intracranial tumors, in particular meningiomas, 
were the most common of these second neoplasms. Monitor all patients with a history of GHD 
secondary to an intracranial neoplasm routinely while on somatropin therapy for progression or 
recurrence of the tumor.
                                 </li>
                                 <li class="cris">
                                        Because children with certain rare genetic causes of short stature have an increased risk of developing 
malignancies, practitioners should thoroughly consider the risks and benefits of starting somatropin in 
these patients. If treatment with somatropin is initiated, carefully monitor these patients for 
development of neoplasms. Monitor patients on somatropin therapy carefully for increased growth, or 
potential malignant changes of preexisting nevi. Advise patients/caregivers to report marked changes in 
behavior, onset of headaches, vision disturbances and/or changes in skin pigmentation or changes in 
the appearance of preexisting nevi.
                                 </li>
                                 <li class="cris">
                                        Treatment with somatropin may decrease insulin sensitivity, particularly at higher doses. New onset 
type 2 diabetes mellitus has been reported in patients taking somatropin. Undiagnosed impaired 
glucose tolerance and overt diabetes mellitus may be unmasked. Monitor glucose levels periodically in 
all patients receiving SKYTROFA. Adjust the doses of antihyperglycemic drugs as needed when 
SKYTROFA is initiated in patients.
                                 </li>
                                  <li class="cris">
                                    Intracranial hypertension (IH) with papilledema, visual changes, headache, nausea, and/or vomiting has 
been reported in a small number of patients treated with somatropin. Symptoms usually occurred within 
the first 8 weeks after the initiation of somatropin and resolved rapidly after cessation or reduction in 
dose in all reported cases. Fundoscopic exam should be performed before initiation of therapy and 
periodically thereafter. If somatropin-induced IH is diagnosed, restart treatment with SKYTROFA at a 
lower dose after IH-associated signs and symptoms have resolved.
                                 </li>
                                  <li class="cris">
                                    Fluid retention during somatropin therapy may occur and is usually transient and dose dependent.
                                 </li>
                                  <li class="cris">
                                    Patients receiving somatropin therapy who have or are at risk for pituitary hormone deficiency(s) may be 
at risk for reduced serum cortisol levels and/or unmasking of central (secondary) hypoadrenalism. 
Patients treated with glucocorticoid replacement for previously diagnosed hypoadrenalism may require 
an increase in their maintenance or stress doses following initiation of SKYTROFA therapy. Monitor 
patients for reduced serum cortisol levels and/or need for glucocorticoid dose increases in those with 
known hypoadrenalism.
                                 </li>
                                  <li class="cris">
                                     Undiagnosed or untreated hypothyroidism may prevent response to SKYTROFA. In patients with GHD, 
central (secondary) hypothyroidism may first become evident or worsen during SKYTROFA treatment. 
Perform thyroid function tests periodically and consider thyroid hormone replacement.
                                 </li>
                                  <li class="cris">
                                    Slipped capital femoral epiphysis may occur more frequently in patients undergoing rapid growth. 
Evaluate pediatric patients with the onset of a limp or complaints of persistent hip or knee pain.
                                 </li>
                                  <li class="cris">
                                    Somatropin increases the growth rate and progression of existing scoliosis can occur in patients who 
experience rapid growth. Somatropin has not been shown to increase the occurrence of scoliosis. 
Monitor patients with a history of scoliosis for disease progression.
                                 </li>
                                  <li class="cris">
                                    Cases of pancreatitis have been reported in pediatric patients receiving somatropin. The risk may be 
greater in pediatric patients compared with adults. Consider pancreatitis in patients who develop 
persistent severe abdominal pain.
                                 </li>
                                  <li class="cris">
                                    When SKYTROFA is administered subcutaneously at the same site over a long period of time, 
lipoatrophy may result. Rotate injection sites when administering SKYTROFA to reduce this risk.
                                 </li>
                                  <li class="cris">
                                    There have been reports of fatalities after initiating therapy with somatropin in pediatric patients with 
Prader-Willi syndrome who had one or more of the following risk factors: severe obesity, history of upper 
airway obstruction or sleep apnea, or unidentified respiratory infection. Male patients with one or more 
of these factors may be at greater risk than females. SKYTROFA is not indicated for the treatment of 
pediatric patients who have growth failure due to genetically confirmed Prader-Willi syndrome.
                                 </li>
                                  <li class="cris">
                                   Serum levels of inorganic phosphorus, alkaline phosphatase, and parathyroid hormone may increase 
after somatropin treatment. 
                                 </li>
                                  <li class="cris">
                                    The most common adverse reactions (≥ 5%) in patients treated with SKYTROFA were: viral infection 
(15%), pyrexia (15%), cough (11%), nausea and vomiting (11%), hemorrhage (7%), diarrhea (6%), 
abdominal pain (6%), and arthralgia and arthritis (6%).
                                 </li>
                                  <li class="cris">
                                    SKYTROFA can interact with the following drugs:
                                 </li>
                                 <ul>
                                    <li class="cris" id="sum">
                                     Glucocorticoids: SKYTROFA may reduce serum cortisol concentrations which may require an 
increase in the dose of glucocorticoids.
                                 </li>
                                  <li class="cris" id="sum">
                                    Oral Estrogen: Oral estrogens may reduce the response to SKYTROFA. Higher doses of SKYTROFA 
may be required.
                                 </li>
                                 <li class="cris" id="sum">
                                    Insulin and/or Other Hypoglycemic Agents: SKYTROFA may decrease insulin sensitivity. Patients 
with diabetes mellitus may require adjustment of insulin or hypoglycemic agents.
                                 </li>
                                  <li class="cris" id="sum">
                                    Cytochrome P450-Metabolized Drugs: Somatropin may increase cytochrome P450 
(CYP450)-mediated antipyrine clearance. Carefully monitor patients using drugs metabolized by 
CYP450 liver enzymes in combination with SKYTROFA.
                                 </li> 
                                 </ul>
                                  
                                 

                            </ul>

<div class="ple">You are encouraged to report side effects to FDA at (800) FDA-1088 or <a class="pre" target="_blank" href=" https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program">www.fda.gov/medwatch</a>. You may 
also report side effects to Ascendis Pharma at 1-844-442-7236.</div>

        <div class="rep">Please <a class="med" target="_blank" href="https://ascendispharma.us/products/pi/skytrofa/skytrofa_pi.pdf"> click here </a>for full Prescribing Information for SKYTROFA.</div>

<hr style="border-top: 2px solid #6c941c;">

<div class="logo123" style="padding">
    <img src="{{asset('sample/img/footer_logo.jpg')}}">
    <div class="con">
     Questions?<a class="con1" href=" http://go.aventriahealth.com/ASCSkytrofaQSAContactForm.html" target="_blank">Contact Us</a>
</div>
</div>




<div class="foo">
   © February  2022 Ascendis Pharma Endocrinology Inc. All rights reserved.<br>
SKYTROFA<sup>®</sup>, Ascendis<sup>®</sup>, Ascendis Signature Access Program<sup>TM</sup>, the Ascendis Pharma logo<br>
and the company logo are trademarks owned by the Ascendis Pharma Group.<br>
US-COMMGHP-2100342 2/22<br>

</div>



                        </div>
                    </div>
                </section>
            </section>


        </form>
        <!-- Main Section Over-->

        <div class="copy-cbd  alert-success">Copy to Clipboard</div>
        <div class="uncopy-cbd alert-danger">Checkbox is not selected</div>



    </div>

    <!-- Modal Starts here-->
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to email the selected resource link
                        to a patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <!--  <div class="modal-body">
                   
                </div> -->
            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->

    <!-- Modal Starts here-->
    <div id="print" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to print the selected tool.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->

    <!-- Modal Starts here-->
    <div id="copy" class="modal fade" role="dialog">
        <div class="modal-dialog modal-dialog-centered">

            <div class="default-box  modal-box">
                <div class="modal-header">
                    <h5 class="modal-title text-center">This button allows the user to copy the link for the selected
                        resource to email to a
                        patient.</h5>
                    <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>

            </div>

        </div>
    </div>
    <!-- Modal Ends Here -->







    <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/sample/plugins.js')}}"></script>
    <script src="{{asset('js/sample/main.js')}}"></script>

</body>

</html>