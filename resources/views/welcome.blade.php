@extends('layout.layout')

@section('title')
 Skytrofaqsa QSA - Quick Support and Access
@endsection

@section('css')
 <link rel="stylesheet" href="{{asset('css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('css/normalize.css')}}">
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
@endsection

@section('content')
<meta name="csrf-token" content="{{ csrf_token() }}">

    <div class="mainBody">




        <div class="grey-bac">
            <section class="container">
                <h6 class="main-topline" id="topline">
                 {!! $content->section_one_content !!}
                 
                </h6>
                
                <div  class="quick-support" >
                    {!! $content->section_two_content !!}
                      
                </div>
             </section>
        </div>
        <section class="container" id="ind">
            <div class="row">

               <!--  <section class="grey-bac"> -->
                <div class="col-12">
                    <!--  <div class="grade-logo"><img src="img/grade.png"></div> -->
             <div class="col-12">
             
          </div>
                </div>

               <div class="col-12"><div class="saf">INDICATION</div>
                <p class="ind_para">SKYTROFA<sup>®</sup> is a human growth hormone indicated for the treatment of pediatric patients 1 year and older 
who weigh at least 11.5 kg and have growth failure due to inadequate secretion of endogenous growth 
hormone (GH).</p>
                <div class="col-12" id="spl">
                    <div class="content">
                        <span class="con-tw">

                        <!--{!! $content->section_two_content !!}-->

                       
                    </span>
                    </div>
                    <div class="imp-content">

                        <h6 class="ind" id="isi">IMPORTANT SAFETY INFORMATION</h6>
                        </div>
                       <ul class="sky">
                           <li class="acutes">
                               SKYTROFA is contraindicated in patients with:
                               <ul>
                                   <li class="acutes">
                                       Acute critical illness after open heart surgery, abdominal surgery or multiple accidental trauma, or if 
                            you have acute respiratory failure due to the risk of increased mortality with use of armacologic 
                            doses of somatropin.
                                   </li>

                                   <li class="acutes grow" >
                                       Hypersensitivity to somatropin or any of the excipients in SKYTROFA. Systemic hypersensitivity 
reactions have been reported with post-marketing use of somatropin products.
                                   </li>
                                   <li class="acutes grow">
                                       Closed epiphyses for growth promotion.
                                   </li>
                                   <li class="acutes grow" >
                                       Active malignancy.
                                   </li>
                                   <li class="acutes grow">
                                       Active proliferative or severe non-proliferative diabetic retinopathy.
                                   </li>
                                   <li class="acutes grow">
                                       Prader-Willi syndrome who are severely obese, have a history of upper airway obstruction or sleep 
apnea or have severe respiratory impairment due to the risk of sudden death.
                                   </li>
                               </ul>
                           </li>

                       </ul>
                       
                        
                       <p class="se" style="color:#4b4b4b; font-size:20px"><strong>
                          Please see Important Safety Information <a href="#note">below</a> and <a target="_blank" href="https://ascendispharma.us/products/pi/skytrofa/skytrofa_pi.pdf"> click here </a> for full Prescribing Information.
                      </strong>
                       </p>
                           
                           
                       
                   

                    {{-- <div class="imp-content">
                         {!! $content->section_three_title !!}
                    </div> --}}
                </div>
            </div>

        </section>

    </div>

    <div class="mainBody sec-btm">
        <section class="container">
            <div class="row flex-sm-row-reverse">

                <div class="col-md-6">
                    <div class="content">
                        <div class="con_one" style="">
                         {!! $pluginContent->content_one !!}
                     </div>
                         <ul class="buttons bdr clr">
                            <li><a href="#" class="btn d-blue gaClick" data-toggle="modal" data-target="#myModal" data-label=" Support Order">Request Plugin</a></li>
                            <li><a href="https://www.research.net/r/pghd-qsa" class="btn gaClick" target="_blank" data-label="GINA">Ask GINA&trade;</a></li>
                            
                        </ul>
                        {!! $pluginContent->content_two !!}
                        <ul class="buttons bdr clr">
                           <?php
                          
                            $urllink = $pluginContent->Leave_behind;
                           
                           ?>
                          <li><a href="#" class="btn gaClick" data-toggle="modal" data-target="#myModal" data-label=" Support Order">Request Order Set Kit</a>
                            </li>
                             <li class="ckk"><a href="<?php echo  $urllink ?>" target="_blank" class="btn gaClick" data-label="Flashcard">View Leave-behind</a>
                            </li>

                        </ul>

                        <p class="note" >GINA is a trademark of Aventria Health Group.</p>
                    </div>
                </div>
                <?php
                  $pass=$pluginContent->image;
              
                //   print_r($pluginContent);
                
                  ?>
                <div class="col-md-6">
                    <div class="img-sec">
                        <a href="{{route('samplePlugin')}}"  target="_blank" class="gaClick" data-label="Sample"><img src="https://web.myskytrofaqsa.com/storage/<?php echo $pass ?>"></a>
                    </div>

                    <ul class="buttons bdr text-center" id="de">
                        <li><a href="{{route('samplePlugin')}}" target="_blank" class="btn gaClick" data-label="Sample">View Live Sample</a>
                        </li>

                    </ul>
                </div>
            </div>
        </section>

    </div>
    <div id="here"></div>
     <div class="mainBody" id="">
        <section class="container"id="#imnote">
            <div class="row" >

                <div class="col-12">


                    <div class="imp-content" >
                        <h6 id="note1">INDICATION</h6>
                        <p class="is-ahun" >
                            SKYTROFA<sup>®</sup> is a human growth hormone indicated for the treatment of pediatric patients 1 year and older 
who weigh at least 11.5 kg and have growth failure due to inadequate secretion of endogenous growth 
hormone (GH).
                        </p>
                        <h6 id="note">IMPORTANT SAFETY INFORMATION</h6>
</div>
                            <ul class="sky" >
                                <li class="acutes">
                                    SKYTROFA is contraindicated in patients with:
                                    <ul>
                                        <li class="acutes">
                                            Acute critical illness after open heart surgery, abdominal surgery or multiple accidental trauma, or if 
you have acute respiratory failure due to the risk of increased mortality with use of armacologic 
doses of somatropin.
                                        </li>
                                        <li class="acutes grow">
                                            Hypersensitivity to somatropin or any of the excipients in SKYTROFA. Systemic hypersensitivity 
reactions have been reported with post-marketing use of somatropin products.
                                        </li>
                                        <li class="acutes grow">
                                            Closed epiphyses for growth promotion.
                                        </li>
                                        <li class="acutes grow">
                                            Active malignancy.
                                        </li>
                                        <li class="acutes grow">
                                            Active proliferative or severe non-proliferative diabetic retinopathy.
                                        </li>
                                        <li class="acutes grow">
                                            Prader-Willi syndrome who are severely obese, have a history of upper airway obstruction or sleep 
apnea or have severe respiratory impairment due to the risk of sudden death
                                        </li>
                                    </ul>
                                </li>
                                <li class="acutes grow">
                                     Increased mortality in patients with acute critical illness due to complications following open heart 
surgery, abdominal surgery or multiple accidental trauma, or those with acute respiratory failure has 
been reported after treatment with pharmacologic doses of somatropin. Safety of continuing SKYTROFA 
treatment in patients receiving replacement doses for the approved indication who concurrently develop 
these illnesses has not been established.
                                </li>

                                <li class="acutes grow">
                                    Serious systemic hypersensitivity reactions including anaphylactic reactions and angioedema have been 
reported with post-marketing use of somatropin products. Do not use SKYTROFA in patients with known 
hypersensitivity to somatropin or any of the excipients in SKYTROFA.
                                </li>
                                <li class="acutes grow">
                                    There is an increased risk of malignancy progression with somatropin treatment in patients with active 
malignancy. Preexisting malignancy should be inactive with treatment completed prior to starting 
SKYTROFA. Discontinue SKYTROFA if there is evidence of recurrent activity.
                                </li>
                                <li class="acutes grow">
                                    In childhood cancer survivors who were treated with radiation to the brain/head for their first neoplasm 
and who developed subsequent growth hormone deficiency (GHD) and were treated with somatropin, an 
increased risk of a second neoplasm has been reported. Intracranial tumors, in particular meningiomas, 
were the most common of these second neoplasms. Monitor all patients with a history of GHD 
secondary to an intracranial neoplasm routinely while on somatropin therapy for progression or 
recurrence of the tumor.
                                </li>
                                <li class="acutes grow">
                                    Because children with certain rare genetic causes of short stature have an increased risk of developing 
malignancies, practitioners should thoroughly consider the risks and benefits of starting somatropin in 
these patients. If treatment with somatropin is initiated, carefully monitor these patients for development 
of neoplasms. Monitor patients on somatropin therapy carefully for increased growth, or potential 
malignant changes of preexisting nevi. Advise patients/caregivers to report marked changes in behavior, 
onset of headaches, vision disturbances and/or changes in skin pigmentation or changes in the 
appearance of preexisting nevi.
                                </li>
                                <li class="acutes grow">
                                   Treatment with somatropin may decrease insulin sensitivity, particularly at higher doses. New onset type 
2 diabetes mellitus has been reported in patients taking somatropin. Undiagnosed impaired glucose 
tolerance and overt diabetes mellitus may be unmasked. Monitor glucose levels periodically in all 
patients receiving SKYTROFA. Adjust the doses of antihyperglycemic drugs as needed when SKYTROFA 
is initiated in patients. 
                                </li>
                                <li class="acutes grow">
                                   Intracranial hypertension (IH) with papilledema, visual changes, headache, nausea, and/or vomiting has 
been reported in a small number of patients treated with somatropin. Symptoms usually occurred within 
the first 8 weeks after the initiation of somatropin and resolved rapidly after cessation or reduction in 
dose in all reported cases. Fundoscopic exam should be performed before initiation of therapy and 
periodically thereafter. If somatropin-induced IH is diagnosed, restart treatment with SKYTROFA at a 
lower dose after IH-associated signs and symptoms have resolved. 
                                </li>
                                <li class="acutes grow">
                                Fluid retention during somatropin therapy may occur and is usually transient and dose dependent.    
                                </li>
                                <li class="acutes grow">
                                    Patients receiving somatropin therapy who have or are at risk for pituitary hormone deficiency(s) may be 
at risk for reduced serum cortisol levels and/or unmasking of central (secondary) hypoadrenalism. 
Patients treated with glucocorticoid replacement for previously diagnosed hypoadrenalism may require 
an increase in their maintenance or stress doses following initiation of SKYTROFA therapy. Monitor 
patients for reduced serum cortisol levels and/or need for glucocorticoid dose increases in those with 
known hypoadrenalism.
                                </li>
                                <li class="acutes grow">
                                   Undiagnosed or untreated hypothyroidism may prevent response to SKYTROFA. In patients with GHD, 
central (secondary) hypothyroidism may first become evident or worsen during SKYTROFA treatment. 
Perform thyroid function tests periodically and consider thyroid hormone replacement. 
                                </li>
                                <li class="acutes grow">
                                    Slipped capital femoral epiphysis may occur more frequently in patients undergoing rapid growth. 
Evaluate pediatric patients with the onset of a limp or complaints of persistent hip or knee pain.
                                </li>
                                 <li class="acutes grow">
                                    Somatropin increases the growth rate and progression of existing scoliosis can occur in patients who 
experience rapid growth. Somatropin has not been shown to increase the occurrence of scoliosis. 
Monitor patients with a history of scoliosis for disease progression.
                                </li>
                                 <li class="acutes grow">
                                    Cases of pancreatitis have been reported in pediatric patients receiving somatropin. The risk may be 
greater in pediatric patients compared with adults. Consider pancreatitis in patients who develop 
persistent severe abdominal pain.
                                </li>
                                 <li class="acutes grow">
                                    When SKYTROFA is administered subcutaneously at the same site over a long period of time, lipoatrophy 
may result. Rotate injection sites when administering SKYTROFA to reduce this risk.
                                </li>
                                 <li class="acutes grow">
                                    There have been reports of fatalities after initiating therapy with somatropin in pediatric patients with 
Prader-Willi syndrome who had one or more of the following risk factors: severe obesity, history of upper 
airway obstruction or sleep apnea, or unidentified respiratory infection. Male patients with one or more 
of these factors may be at greater risk than females. SKYTROFA is not indicated for the treatment of 
pediatric patients who have growth failure due to genetically confirmed Prader-Willi syndrome.
                                </li>
                                 <li class="acutes grow">
                                    Serum levels of inorganic phosphorus, alkaline phosphatase, and parathyroid hormone may increase 
after somatropin treatment.
                                </li>
                                <li class="acutes grow">
                                 The most common adverse reactions (≥ 5%) in patients treated with SKYTROFA were: viral infection 
(15%), pyrexia (15%), cough (11%), nausea and vomiting (11%), hemorrhage (7%), diarrhea (6%), 
abdominal pain (6%), and arthralgia and arthritis (6%).   
                                </li>
                                <li class="acutes grow">
                                    SKYTROFA can interact with the following drugs:
                                </li>
                                <ul>
                                    <li  class="acutes grow">
                                         Glucocorticoids: SKYTROFA may reduce serum cortisol concentrations which may require an 
increase in the dose of glucocorticoids.
                                    </li>
                                    <li  class="acutes grow">
                                        Oral Estrogen: Oral estrogens may reduce the response to SKYTROFA. Higher doses of SKYTROFA 
may be required.
                                    </li>
                                     <li  class="acutes grow">
                                        Insulin and/or Other Hypoglycemic Agents: SKYTROFA may decrease insulin sensitivity. Patients with 
diabetes mellitus may require adjustment of insulin or hypoglycemic agents.
                                    </li>
                                     <li  class="acutes grow">
                                        Cytochrome P450-Metabolized Drugs: Somatropin may increase cytochrome P450 
(CYP450)-mediated antipyrine clearance. Carefully monitor patients using drugs metabolized by 
CYP450 liver enzymes in combination with SKYTROFA.
                                    </li>

                                </ul>
                            </ul>
                       
                       <!--  <h6>IMPORTANT SAFETY INFORMATION</h6> -->
                       <!--  <ul class="mb-4"

                        </ul> -->
                         <div class="imp-content" >
                           <p class="en-ged"><strong>You are encouraged to report side effects to FDA at (800) FDA-1088 or <a class="ing" href=" https://www.fda.gov/safety/medwatch-fda-safety-information-and-adverse-event-reporting-program" target="_blank">www.fda.gov/medwatch</a> .You may 
also report side effects to Ascendis Pharma at 1-844-442-7236.</strong></p>

                       <p class="en-ged"><strong>
                           Please <a target="_blank" href="https://ascendispharma.us/products/pi/skytrofa/skytrofa_pi.pdf">click here </a> for full Prescribing Information for SKYTROFA
                       </strong></p>
                     
                    </div>
                    
                    <hr class="grhr">
                
                   <!--  <div class="imp-content ref">
                        <p><strong>References: 1.</strong> Ehringer G, Duffy B. Promoting best practice and safety
                            through preprinted physician orders. In:
                            Henriksen K, Battles JB, Keyes MA, et al, eds.<i> Advances in Patient Safety: New Directions
                                and Alternative Approaches
                                (Vol. 2: Culture and Redesign)</i>. Rockville, MD: Agency for Healthcare Research and
                            Quality; 2008.</p>
                    </div> -->
                </div>
            </div>

        </section>

    </div>
@endsection

@section('js')
  <script src="{{asset('js/vendor/modernizr-3.5.0.min.js')}}"></script>
    <script src="{{asset('js/vendor/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('js/bootstrap.js')}}"></script>
    <script src="{{asset('js/plugins.js')}}"></script>
    <script src="{{asset('js/main.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.19.2/dist/jquery.validate.js"></script>
<script type="text/javascript">

$("#email-form").validate({
    rules:{
        type_of_organization:{
                required:true
            },
        tnc:{
            required:true
        },
          quick_support:{
            required:true
        }
    },
        messages: {
            name: "Please enter your name",
            email: "Please enter your email",
            professional: "Please enter your professional title",
            organization: "Please enter your organization's name",
            city: "Please enter your organization's city",
            state: "Please enter your organization’s state",
            type_of_organization:"Please select at least one type of organization",
            tnc:"Please certify you are a healthcare provider",
             quick_support:"Please select at least one checkbox"

        }
    });
    var quick_support; 
          $('#quick_support').on('change', function() {
                 quick_support = this.checked ? this.value : '';
                  
           console.log(quick_support);
                });
     
      var order_set_kit; 
          $('#order_set_kit').on('change', function() {
                 order_set_kit = this.checked ? this.value : '';
                  
           console.log(order_set_kit);
                });

    $(document).on('click','#send_email', function(e){
        // get the values
       
        var name = $('#name').val();
        var email = $('#email').val();
        var professional_title = $('#professional_title').val();
        var name_of_organization = $('#name_of_organization').val();
        var organization_city = $('#organization_city').val();
        var organization_state = $('#organization_state').val();
         var manager_name = $('#manager_name').val();
        var type_of_organization = $("input[name='type_of_organization']:checked").val();
        if(type_of_organization == 'other')
        {
            type_of_organization = $("#other").val();
        }
        var tandc = $('#tandc').val();
        // console.log(quick_support,order_set_kit,name,email,professional_title,name_of_organization,organization_city,organization_state,type_of_organization);
       
      

        if($('#email-form').valid())
        {
           
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                } 
            });
            $.ajax({
              
                url : "{{route('plugin.form.submit')}}",
                type: "POST",
                data : {'quick_support' : quick_support, 'order_set_kit' : order_set_kit, 'name' : name, 'email' : email, 'professional_title' : professional_title, 'name_of_organization' : name_of_organization, 'organization_city' : organization_city, 'organization_state' : organization_state,'manager_name': manager_name, 'type_of_organization' : type_of_organization},
                success: function(response)
                {   
                     console.log(response)
                     if(response.status == 200) {
                   
                  
                        $('#myModal').removeClass('show');
                        $('#myModal-2').addClass('show');
                        $('#myModal-2').css('display','block');
                     }
                    // return (response)
                },
                error: function (jqXHR, textStatus, errorThrown)
                {
             
                }
            });
        }
        
        e.preventDefault();
    });
function myFunction() {
  $('.close').on('click', function () {
            $('.modal-backdrop').remove();
            $(this).parents('#myModal-2').hide();
            location.reload();
        })
}
// $('.gaClick').click(function(e){
//             // google analytics click functionality
//           var label = $(this).attr('data-label');
//           var test = ga('send', {
//              hitType: 'event',
//              eventCategory: label,
//              eventAction:  label,
//              eventLabel: label
//           });

//     });
// $('.gaPi').click(function(e){
//             // google analytics click functionality
//           var label = 'PI';
//           var test = ga('send', {
//              hitType: 'event',
//              eventCategory: 'PI',
//              eventAction:  label,
//              eventLabel: label
//           });

//     });
// $('.gaIsi').click(function(e){
//             // google analytics click functionality
//           var label = 'ISI';
//           var test = ga('send', {
//              hitType: 'event',
//              eventCategory: 'ISI',
//              eventAction:  label,
//              eventLabel: label
//           });

//     });
</script>
<script>
    var position = $(window).scrollTop(); 

// should start at 0

$(window).scroll(function() {
    var scroll = $(window).scrollTop();
    if(scroll > position) {
        console.log('scrollDown');
        // $('div').text('Scrolling Down Scripts');
    } else {
         console.log('scrollUp');
        //  document.getElementById("ide").style.margin-top:"3%";
        //  $('div').text('Scrolling Up Scripts');
        
    }
    position = scroll;
});
</script>
@endsection