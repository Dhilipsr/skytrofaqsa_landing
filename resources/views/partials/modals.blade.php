<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<div id="myModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog modal-box">
        <div class="default-box">
            <div class="modal-header">

                <button type="button" class="close model-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-12">
                            <h4 class="heading-text">SKYTROFA<sup>®</sup> (lonapegsomatropin-tcgd)<br>
Quick Support & Access Order Form
                            </h4>
                            <h6 class="model-title">Simple steps to order:</h6>
                            <ul class="model-list">
                                <li><span>1)</span> Complete the registration information below and hit Submit.</li>
                                <li><span>2)</span> A member of the QSA team will contact you to confirm your order.</li>
                                <li><span>3)</span> Your order will be sent to you in 1 week (5 business days).
                                </li>
                            </ul>
                            <p class=bl-ld>If you have any questions, please contact <a class="em1" 
                        href="mailto:support@pghdsupport.com"
                                    target="_black">support@pghdsupport.com.</a></p>

                        </div>
                    </div>
                </div>
                <div class="mainBody sec-bt fg" style="">
                    <section class="container">
                        <div class="row">
                            <div class="col-12">


                                <p class="form-title">Please select which item(s) you are interested in:</p>
                                <form class="form-validate" id="email-form" >


                                    <label class="container-checkbox">SKYTROFA Quick Support & Access Plugin
                                        <input type="checkbox" id="quick_support" name="quick_support" value="quick_support">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="container-checkbox">Order Set Kit
                                        <input type="checkbox" id="order_set_kit" name="quick_support" value="order_set_kit">
                                        <span class="checkmark"></span>
                                    </label>

                                    <div class="noted">
                                        <label><span>*</span>Indicates required field</label>

                                    </div>


                                    <div class="form-group req">
                                        <p class="incre-se">Your name</p>
                                        <input id="name" type="text" class="form-control"  name="name" required>
                                        {{-- <div class="invalid-feedback">Please enter your name</div> --}}
                                        <b id="name_error"></b>
                                    </div>
                                    <div class="form-group req">
                                        <p class="incre-se">Your email</p>
                                        <input id="email" type="email" name="email" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your email</div> --}}
                                        <b id="email_error"></b>
                                    </div>

                                    <div class="form-group req">
                                        <p class="incre-se">Your professional title</p>
                                        <input id="professional_title" type="text" name="professional" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your professional title</div> --}}
                                        <b id="professional_title_error"></b>
                                    </div>

                                    <div class="form-group req">
                                        <p class="incre-se">The name of your organization</p>
                                        <input id="name_of_organization" type="text" name="organization" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your organization's name
                                        </div> --}}
                                         <b id="name_of_organization_error"></b>
                                    </div>

                                    <div class="form-group req">
                                        <p class="incre-se">Your organization’s city</p>
                                        <input id="organization_city" type="text" name="city" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your organization's city
                                        </div> --}}
                                        <b id="organization_city_error"></b>
                                    </div>

                                    <div class="form-group req">
                                        <p class="incre-se">Your organization’s state</p>
                                        <input id="organization_state" type="text" name="state" class="form-control" required>
                                        {{-- <div class="invalid-feedback">Please enter your organization’s state
                                        </div> --}}
                                        <b id="organization_state_error"></b>
                                    </div>
                                    <div class="form-group">
                                        <label class="ur-sky skky" style="">Your Skytrofa Account Manager’s name <span style="color: #a02c74;">(optional)</span></label>
                                        <input id="manager_name" name="manager_name" type="text" class="form-control" >
                                        {{-- <div class="invalid-feedback">Please enter your organization’s state </div> --}}
                                    </div>
                                    <div class="form-group req mb-2">
                                        <p class="incre-se">Type of organization</p>
                                    </div>
                                    <label class="container-checkbox ur-sky">Large Group Practice or Physician Group

                                        <input type="radio" class="type_of_organization" value="Large Group Practice or Physician Group" name="type_of_organization">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="container-checkbox ur-sky">Health System
                                        <input type="radio" class="type_of_organization" value="Health System" name="type_of_organization">
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="container-checkbox ur-sky" style="">

                                        Other:
                                        <input type="radio" value="other" name="type_of_organization">
                                        <span class="checkmark"></span>
                                        <input type="text" id="other" class="other type_of_organization">


                                    </label>
                                    <b id="type_of_organization_state_error"></b>



                                    <div class="form-action   ">
                                        <label class="container-checkbox ur-sky">I hereby certify I am a healthcare
                                            provider.<sup style="color:red; font-size: 0.9em;">*</sup>
                                            <input type="checkbox" id="tandc" name="tnc">
                                            <span class="checkmark"></span>
                                            <div class="cs"><p class="las">Please note that at Ascendis Pharma, we respect your rights to privacy and we therefore recommend you see our <a target="_blank" href="https://ascendispharma.us/privacy-policy/"><span style="color: #a02c74;text-decoration: none;">Privacy Policy</span></a> to learn how we process your personal data and also learn more about your rights.</p>
                                            <p>By clicking “Submit,” I confirm that I have given my consent to the collection and processing of the information provided to monitor and address questions about the QSA product and to improve and maintain it. You may at any time withdraw your consent. For more information, please see our <a target="_blank" href="https://ascendispharma.us/privacy-policy/"><span style="color: #a02c74;text-decoration: none;">Privacy Policy</span></a>.</p></div>
                                        </label>
                                            <div class="g-recaptcha" data-sitekey="6LezB8oeAAAAAJHgrEbwTuYi1s9f2f9HR5cHcvfI" data-callback="verifyCaptcha" onclick="myFunction()"></div>
                                             <div id="demo"></div>
                                          </div>
                                        <b id="tandc_error"></b>
                                         <div class="form-action">
                                        <button type="submit" class="model-btn1" id="send_email">Submit</button>
                                        
                                        </div>
                                         <div class="adj2">
                                       
                                      </div>
                                    </div>

                                </form>
                                
                        </div>
                        <div class="d-flex align-items-sm-center">
                             <button class="gh" onclick="verify()">Submit</button>
                        </div>
                       
                    </section>
                    
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.gh').submit(function(e){
        // window.location.reload(false);
        // e.preventDefault();
    });
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
    async defer>
</script>

<script>
    function verify()
    {
        //   window.location.reload(false);
      document.getElementById("demo").innerHTML = "The reCAPTCHA wasn't entered correctly!";
    //   e.preventDefault();
      
    }
</script>

<script type="text/javascript">

$('.model-btn1').hide();

  function verifyCaptcha() { 
   
      $('.gh').hide();
      $('#demo').hide();
      $('.model-btn1').show();
    
  };
   
</script>
<style>
    button.gh {
    /*margin-bottom: 6em;*/
    /*margin-left: 19em;*/
    background: #a02c74;
    padding-right: 72px;
    padding-left: 77px;
    padding-top: 12px;
    padding-bottom: 13px;
    color: white;
    font-size: 18px;
    font-weight: 600;
    border-radius: 42px;
 /*margin-left: 353px !important;*/
    margin-top: -16px;
    margin-bottom: 45px !important;
    border: solid #a02c74;
    
}
button#send_email
{
    /*margin-bottom: 6em;*/
    /*margin-left: 19em;*/
    background: #a02c74;
    padding-right: 72px;
    padding-left: 77px;
    padding-top: 13px;
    padding-bottom: 14px;
    color: white;
    font-size: 18px;
    font-weight: 600;
    border-radius: 42px;
    /*margin-bottom: 36px;*/
    margin-left: 80px;
    margin-top: 0px;
    /*margin-top:18px; after recapcha implete use this top margin css*/
    border: cadetblue;
}
div#demo {
    padding-left: 0em;
    padding-top: 1em;
    color: #dc3545;
}



@media (min-width: 1228px)
{
.d-flex.align-items-sm-center {
  justify-content: center;
  margin-right:8em;
}
}
.d-flex.align-items-sm-center {
  justify-content: center;
}
/*@media (min-width: px)*/
/*{*/
/*.d-flex.align-items-sm-center {*/
/*    margin-left: -10em;*/
/*}*/
/*}*/
</style>

<div id="myModal-2" class="modal hide fade" tabindex="-1" data-replace="true">
    <div class="modal-dialog modal-box ">
        <div class="default-box">
            <div class="modal-body">
                 <button type="button" onclick="myFunction()" class="close model-close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
                <div class="mainBody sec-btmm re">
                   
                    <section class="container">
                        <div class="row">
                            <div class="col-12 text-center">
                                <h4 class="headtxt">SKYTROFA<sup>®</sup>
                                (lonapegsomatropin-tcgd)<br> 
Quick Support & Access Order Form
                                </h4>

                                <p class="sm" style="color: #4b4b4b; "><strong style="font-size:23px;">Thank you for ordering SKYTROFA Quick Support & Access!<br>
You should expect an email within the next 48 hours with
more information and next steps.</strong>
                                </p>


                            </div>
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
</div>